import React, {Component} from 'react';
import OurFlatList from './app/components/ourFlatList/OurFlatList';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Button,
} from 'react-native';

function HomeScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Details"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}

function Lista({navigation}) {
  return (
    <OurFlatList navigation={navigation}/>
  );
}

function DetailsScreen({route, navigation}) {

  const {itemTitle} = route.params;
  const {itemID} = route.params;
  const {itemDesc} = route.params;
  const {itemPic} = route.params;

  return (
    <View style={{backgroundColor: 'brown', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      
        <Text style={{color: 'black' , fontSize: 35,fontWeight: 'bold', marginBottom: 20}}>{itemTitle}</Text>
        <Image style={{aspectRatio: 2/3 , width:250 , marginBottom: 20, borderRadius: 25} } source={{uri: itemPic}} />
        <Text style={{color: 'yellow' , fontSize: 20, marginBottom: 20, marginLeft: 30, marginRight: 30}}>Descripcion: {itemDesc}</Text>

      <TouchableOpacity onPress = {() => navigation.popToTop()}>
        <View style = {{padding:10, backgroundColor: 'black', alignItems: 'center', justifyContent: 'center', borderRadius: 25}}>
          <Text style = {{fontSize: 20, color: 'white'}}>Regresar</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createStackNavigator();

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: 0,
      count: 0,
      items: [],
      error: null,
    };
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="List" component={Lista}/>
          <Stack.Screen name="Details" component={DetailsScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button: {
    top: 10,
    fontSize: 50,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },

  container: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    backgroundColor: 'orange',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
